#include <iostream>
#include <array>

// Some global definitions
constexpr int N_DIMENSIONS = 4;
constexpr float DEFAULT_STRENGTH = 0.1f;
constexpr int DEFAULT_N_IT = 100;
constexpr std::array<float, N_DIMENSIONS> DEFAULT_WEIGHTS = {1.0f, 1.0f, 0.0f, 0.0f};

#ifndef IMAGE_PATH
#define IMAGE_PATH "./resources/test.bmp"
#endif

#ifndef OUTPUT_PATH
#define OUTPUT_PATH "./resources/testOut.bmp"
#endif

// Helper classes
#include "image.hpp"
#include "CmdArgParser.hpp"
#include "progressBar.hpp"

// Some struct to organise data inside the kernel
typedef struct SubS {
   int x;
   int y;
   int z;
   int t;
   int w;
} Sub;

__device__ Sub i2s(int i, int X, int XY, int XYZ, int XYZT, int XYZTW){
	Sub s = {i%X, (i%XY)/X, (i%XYZ)/XY, (i%XYZT)/XYZ, (i%XYZTW)/XYZT};
	return s;
}

__device__ int s2i(int x, int y, int z, int t, int w, int X, int Y, int Z, int T, int W, int XY, int XYZ, int XYZT){
    return ((x+X)%X) + ((y+Y)%Y) * X + ((z+Z)%Z) * XY + ((t+T)%T) * XYZ  + ((w+W)%W) * XYZT; // Added +X to modulo negative numbers properly. assumes that x wont be lower than -X!
}

// 58ms 128^3x16 GTX970, 10ms 128^3x16 GTX TITAN X
__global__ void Ppf( float *in,
                     float *out,
                     int    X,
                	 int    Y,
                 	 int    Z,
                 	 int    T,
                 	 int    W,
                 	 int    XY,
                 	 int    XYZ,
                 	 int    XYZT,
                 	 int    XYZTW){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < XYZTW){
    	const Sub s = i2s(i, X, XY, XYZ, XYZT, XYZTW);
		float k[4] = {0 < W? 1.0f: 0.0f, 1 < W? 1.0f: 0.0f, 2 < W? 1.0f: 0.0f, 3 < W? 1.0f: 0.0f};
    	float pqsqr = sqrt(+k[0] * powf(in[(i + (XYZT * 0))%XYZTW], 2)
						   +k[1] * powf(in[(i + (XYZT * 1))%XYZTW], 2)
						   +k[2] * powf(in[(i + (XYZT * 2))%XYZTW], 2)
						   +k[3] * powf(in[(i + (XYZT * 3))%XYZTW], 2));;

    	out[i] = in[i] / max(1.0f, pqsqr);
    }
}

// 12ms 128^3x16 GTX970, 7ms 128^3x16 GTX TITAN X
__global__ void YmLaL( float *in,
                       float *out,
                       float *p,
                       float  lambda,
                 	   int   	X,
                 	   int    Y,
                 	   int    Z,
                 	   int    T,
                 	   int    W,
                 	   int   	XY,
                 	   int   	XYZ,
                 	   int    XYZT,
                 	   int    XYZTW,
                       float  w0,
                       float  w1,
                       float  w2,
                       float  w3){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < XYZT){
    	const Sub s  = i2s(i, X, XY, XYZ, XYZT, XYZT);

		float k[4] = {0 < W? 1.0f: 0.0f, 1 < W? 1.0f: 0.0f, 2 < W? 1.0f: 0.0f, 3 < W? 1.0f: 0.0f};
    	float dsum = +k[0] * w0 * in[(i + XYZT * 0)%XYZTW]
    				 +k[1] * w1 * in[(i + XYZT * 1)%XYZTW]
    				 +k[2] * w2 * in[(i + XYZT * 2)%XYZTW]
    				 +k[3] * w3 * in[(i + XYZT * 3)%XYZTW]
    				 -k[0] * w0 * in[s2i(s.x - 1, s.y    , s.z    , s.t    , 0, X, Y, Z, T, W, XY, XYZ, XYZT)]
					 -k[1] * w1 * in[s2i(s.x    , s.y - 1, s.z    , s.t    , 1, X, Y, Z, T, W, XY, XYZ, XYZT)]
					 -k[2] * w2 * in[s2i(s.x    , s.y    , s.z - 1, s.t    , 2, X, Y, Z, T, W, XY, XYZ, XYZT)]
					 -k[3] * w3 * in[s2i(s.x    , s.y    , s.z    , s.t - 1, 3, X, Y, Z, T, W, XY, XYZ, XYZT)];

        out[i] = p[i] - lambda*dsum;
    }
}

// 38ms 128^3x16 GTX970, 21ms 128^3x16 GTX TITAN X
__global__ void RskpKLT( float *in,
                         float *out,
                         float *rsk,
                      	 float  lambda,
                 	     int    X,
                 	     int    Y,
                 	     int    Z,
                 	     int    T,
	                 	 int    W,
                 	     int    XY,
                 	     int    XYZ,
                 	     int    XYZT,
                 	     int    XYZTW,
                         float  w0,
                         float  w1,
                         float  w2,
                         float  w3){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < XYZTW){
    	const Sub s  = i2s(i, X, XY, XYZ, XYZT, XYZTW);
    	int sd[4] = {s.x, s.y, s.z, s.t};
    	    sd[s.w] += 1;
        float we[4] = {w0, w1, w2, w3};

    	float lft = +in[s2i(  s.x,   s.y,   s.z,   s.t, 0, X, Y, Z, T, 1, XY, XYZ, XYZT)]
    				-in[s2i(sd[0], sd[1], sd[2], sd[3], 0, X, Y, Z, T, 1, XY, XYZ, XYZT)];

        out[i] = rsk[i] + we[s.w] * lft / 4 / W / lambda;
    }
}

// 0ms 128^3x16 GTX970, 0ms 128^3x16 GTX TITAN X
__global__ void Pcf( float *in,
                     float *out,
                     int    XYZT){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < XYZT){
        out[i] = max((float)in[i], 0.0f);
    }
}

// 10ms 128^3x16 GTX970, 6ms 128^3x16 GTX TITAN X
__global__ void PqpKPm1( float *out,
                         float *pqk,
					     float *pqkm1,
                      	 float  tk,
					   	 float  tkp1,
                 	     int    XYZTW){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < XYZTW){
    	out[i] = pqk[i] + (tk - 1)/tkp1*(pqk[i] - pqkm1[i]);
    }
}

// 7ms 128^3x16 GTX970, 4ms 128^3x16 GTX TITAN X
__global__ void imCpy( float *in,
                       float *out,
                       int    nele){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i < nele){
        out[i] = in[i];
    }
}

// Main program
int main(int argc, char* argv[]) {
	// Parse arguments
    CmdArgParser argIn("denoise-tv", argc, argv);
    argIn.printAndQuitIfHelp();

    // Read image
	Image im(argIn.getPathIn());

    // Parameters
    float lambda = argIn.getStrength();
    int X = im.getSizeX();
    int Y = im.getSizeY();
    int Z = im.getSizeZ();
    int T = im.getSizeT();
    int W = N_DIMENSIONS;
    std::array<float, 4> weights = argIn.getWeights();
    int IM_SIZE = X * Y * Z * T;
    int GRADIENT_SIZE = IM_SIZE * N_DIMENSIONS;
    int nIt = argIn.getNIterations();

    std::cout << "Loading image from path: " << argIn.getPathIn() << ", Size: " << im.getData().size() << " (" << X << ", " << Y << ", " << Z << ", " << T << ")" << std::endl;

    // Host memory
    std::vector<float> memY_host       = im.getData();
    float* memPqkm1_host   = new float[GRADIENT_SIZE]();
    float* memPqk_host     = new float[GRADIENT_SIZE]();
    float* memRsk_host     = new float[GRADIENT_SIZE]();
    float* memYmLaL_host   = new float[IM_SIZE]();
    float* memRskpKLT_host = new float[GRADIENT_SIZE]();
    float* memPcf_host     = new float[IM_SIZE]();

    // GPU Memory
    float *memY, *memPqkm1, *memPqk, *memRsk, *memYmLaL, *memRskpKLT, *memPcf;
    cudaMalloc(&memY      , IM_SIZE       * sizeof(float));
	cudaMalloc(&memPqkm1  , GRADIENT_SIZE * sizeof(float));
	cudaMalloc(&memPqk    , GRADIENT_SIZE * sizeof(float));
	cudaMalloc(&memRsk    , GRADIENT_SIZE * sizeof(float));
	cudaMalloc(&memYmLaL  , IM_SIZE       * sizeof(float));
	cudaMalloc(&memRskpKLT, GRADIENT_SIZE * sizeof(float));
	cudaMalloc(&memPcf    , IM_SIZE       * sizeof(float));

    // Init memory
    cudaMemcpy(memY      , memY_host.data() , IM_SIZE       * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memPqkm1  , memPqkm1_host    , GRADIENT_SIZE * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memPqk    , memPqk_host      , GRADIENT_SIZE * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memRsk    , memRsk_host      , GRADIENT_SIZE * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memYmLaL  , memYmLaL_host    , IM_SIZE       * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memRskpKLT, memRskpKLT_host  , GRADIENT_SIZE * sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(memPcf    , memPcf_host      , IM_SIZE       * sizeof(float) , cudaMemcpyHostToDevice);

    int threadPerBlock = 256;
    int nBlockIm = ceil(float(IM_SIZE) / threadPerBlock);
    int nBlockGr = ceil(float(GRADIENT_SIZE) / threadPerBlock);

    float tk = 1;
	ProgressBar pr(nIt);
    for (int i = 0; i < nIt; i++){
        YmLaL <<< nBlockIm, threadPerBlock>>> (memRsk,
                                               memYmLaL,
                                               memY,
                                               lambda,
                                               X, Y, Z, T, W,
                                               X*Y, X*Y*Z, X*Y*Z*T, X*Y*Z*T*W,
                                               weights[0], weights[1], weights[2], weights[3]);

        // For later: Positive constraint
        // Pcfg <<< nBlockIm, threadPerBlock>>> (memRsk)

        RskpKLT <<< nBlockGr, threadPerBlock>>> (memYmLaL,
                                                 memRskpKLT,
                                                 memRsk,
                                                 lambda,
                                                 X, Y, Z, T, W,
                                                 X*Y, X*Y*Z, X*Y*Z*T, X*Y*Z*T*W,
                                                 weights[0], weights[1], weights[2], weights[3]);

        Ppf <<< nBlockGr, threadPerBlock>>> (memRskpKLT,
                                             memPqk,
                                             X, Y, Z, T, W,
                                             X*Y, X*Y*Z, X*Y*Z*T, X*Y*Z*T*W);

        float tkp1 = (1 + std::sqrt(1 + 4 * std::pow(tk, 2))) / 2;

        PqpKPm1 <<< nBlockGr, threadPerBlock >>> (memRsk,
                                                 memPqk,
                                                  memPqkm1,
                                                 tk,
                                                 tkp1,
                                                 X*Y*Z*T*W);

		if (!argIn.getIsSilent()) pr.tic();
    }

    YmLaL <<< nBlockIm, threadPerBlock >>> (memPqk,
                                            memYmLaL,
                                            memY,
                                            lambda,
                                            X, Y, Z, T, W,
                                            X*Y, X*Y*Z, X*Y*Z*T, X*Y*Z*T*W,
                                            weights[0], weights[1], weights[2], weights[3]);


    std::vector<float> result_host(IM_SIZE);
	cudaMemcpy(result_host.data(), memYmLaL, IM_SIZE * sizeof(float), cudaMemcpyDeviceToHost);

    Image resultIm(result_host, X, Y);
    resultIm.writeImage(argIn.getPathOut());

    return 0;
}
