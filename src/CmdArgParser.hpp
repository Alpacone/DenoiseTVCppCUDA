#ifndef CMDARGPARSER_H_
#define CMDARGPARSER_H_

#include "argparse/include/argparse/argparse.hpp"

/**
 * Class for parsing arguments for denoise-tv
 */
class CmdArgParser {
private:
	argparse::ArgumentParser program;

	std::string m_pathIn;
	std::string m_pathOut;
	float m_strength;
	size_t m_nIterations;
	std::array<float, N_DIMENSIONS> m_weights;
	bool m_isSilent;
	bool m_isHelp;
public:
    // Getters
	std::string getPathIn();
	std::string getPathOut();
	float getStrength();
	size_t getNIterations();
	std::array<float, N_DIMENSIONS> getWeights();
	bool getIsSilent();
	bool getIsHelp();

    /** Checks if help flag is specified. If so,
     * prints the help text and quits
     */
    void printAndQuitIfHelp();

    /**
     * Only constructor allowed is this
     * It takes in the arguments as passed to main and parses them
     */
    CmdArgParser(std::string programName, int argc, char** argv);

    CmdArgParser() = delete;
    ~CmdArgParser() = default;
    CmdArgParser(const CmdArgParser&) = delete;
    CmdArgParser(CmdArgParser&&) = delete;
    CmdArgParser& operator=(const CmdArgParser&) = delete;
    CmdArgParser& operator=(CmdArgParser&&) = delete;
};

CmdArgParser::CmdArgParser(std::string programName, int argc, char** argv){
	// Init agument parser
	argparse::ArgumentParser program("denoise-tv");

	// Add the positional arguments
	program.add_argument("pathIn")
		.help("The path to the image to read")
        .default_value(IMAGE_PATH);

	program.add_argument("pathOut")
		.help("The path to the denoised output image")
        .default_value(OUTPUT_PATH);

	program.add_argument("-s", "--silent")
		.help("Wether to display progress of the algorithm")
		.default_value(false)
		.implicit_value(true);

	program.add_argument("-h", "--help")
		.help("Displays the help text")
		.default_value(false)
		.implicit_value(true);

	program.add_argument("-l", "--lambda", "--strength")
		.help("Denoising strength. Too high values can result in a paint brush like effect")
		.scan<'g', float>()
		.default_value(DEFAULT_STRENGTH);

	program.add_argument("-n", "--nIt")
		.help("Number of iterations to perform. More iterations take longer but produces more accurate results. Higher denoising strengths may require more iterations.")
		.scan<'i', int>()
		.default_value(DEFAULT_N_IT);

    program.add_argument("-w", "--weights")
        .help("Denoising strenght weights in each dimension of the image. Best beween 0 and 1. Best to leave this at deafult values otherwise may result in weird looking images")
        .nargs(N_DIMENSIONS)
        .scan<'g', float>()
        .default_value(std::vector<float>(DEFAULT_WEIGHTS.begin(), DEFAULT_WEIGHTS.end()));

	try {
		program.parse_args(argc, argv);
	} catch (const std::runtime_error& err) {
		std::cerr << err.what() << std::endl;
		std::cerr << program;
		std::exit(1);
	}

    std::array<float, N_DIMENSIONS> arrWeights;
    std::copy_n(program.get<std::vector<float>>("--weights").begin(), N_DIMENSIONS, arrWeights.begin());

    m_pathIn = program.get<std::string>("pathIn"),
    m_pathOut = program.get<std::string>("pathOut"),
    m_strength = program.get<float>("--strength"),
    m_nIterations = (size_t)program.get<int>("--nIt"),
    m_weights = arrWeights,
    m_isSilent = program.get<bool>("--silent"),
    m_isHelp = program.get<bool>("--help");
}

void CmdArgParser::printAndQuitIfHelp(){
    if (getIsHelp()){
        std::cout << program;
        std::exit(0);
    }
}

// Getters
std::string CmdArgParser::getPathIn(){
    return m_pathIn;
}

std::string CmdArgParser::getPathOut(){
    return m_pathOut;
}

float CmdArgParser::getStrength(){
    return m_strength;
}

size_t CmdArgParser::getNIterations(){
    return m_nIterations;
}

std::array<float, N_DIMENSIONS> CmdArgParser::getWeights(){
    return m_weights;
}

bool CmdArgParser::getIsSilent(){
    return m_isSilent;
}

bool CmdArgParser::getIsHelp(){
    return m_isHelp;
};

#endif // CMDARGPARSER_H_
