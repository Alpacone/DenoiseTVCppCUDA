#ifndef PROGRESSBAR_H_
#define PROGRESSBAR_H_

#include <chrono>
#include <cstddef>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>

using namespace std::chrono_literals;

/**
 * Displays the progress of some computation or loop and gives an ETA.
 */
class ProgressBar {
private:
	const size_t nItTot = 1; /** Total number iterations to be performed */
	size_t nItCurrent = 0; /** Number of iterations that have been completed so far */
	const std::chrono::steady_clock::time_point tStart = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsedTime;
    std::chrono::steady_clock::time_point currentTime = tStart;
    std::chrono::steady_clock::time_point timeOfLastPrint = tStart;
    bool finished = false;

	static constexpr double updateFrequency = 20; /** How often to print the progress bar (Hz)*/
    static constexpr int progressPrintSquares = 30;
    static constexpr bool deleteCurrentLine = true;

    /**
     * Converts number of iterations to a predicted duration
     */
    std::chrono::duration<double> nItToPredictedTime(size_t nIt);

    /**
     * Prints the progress bar
     */
    void print();

    /**
     * Creates the years months weeks days hours minutes seconds string */
    std::string createYmwdhmsString(std::chrono::duration<double> time);

    /**
     * Determines wether enough time has passed to update the status bar
     */
    bool shouldPrint();

public:
	/**
	 * Increments the number of iterations by n
	 * \param n number of times to increment the number of iterations
	 */
	void tic(size_t n = 1);

    /**
     * Finishes the iteration progress and shows the total run time
     */
    void finalise();

	/**
	 * Constructs the object with nIt number of total iterations.
	 * \param nIt Total number of iterations that will be performed.
	 */
	ProgressBar(size_t nIt);

	/**
	 * Fetches the average duration of an iteration so far
	 */
	std::chrono::duration<double> getAverageTimePerIteration();

	/**
	 * Fetches the time elapsed since until last tick
	 */
	std::chrono::duration<double> getElapsedTime();

	/**
	 * Fetches the total number of iterations
	 */
	size_t getNItTot();

	/**
	 * Fetches the number of iterations that happened so far
	 */
	size_t getNItCurrent();

	/**
	 * Fetches the number of remaining iterations
	 */
	size_t getNItRemaining();

    /**
     * Predicts the total run time
     */
    std::chrono::duration<double> predictTotalRunTime();

    /**
     * Predicts the remaining run time
     */
    std::chrono::duration<double> predictRemainingRunTime();

    /**
     * Calculates the progress as a fraction of completed iterations and total iterations
     */
    float getCompletionFraction();
};

ProgressBar::ProgressBar(size_t nIt)
	: nItTot(nIt) {
}

void ProgressBar::tic(size_t n) {
	// If finished do nothing
	if (finished)
		return;

	// Update number of iterations
	nItCurrent += n;

	// Get current time
	currentTime = std::chrono::steady_clock::now();

	// Update elapsed time
	elapsedTime = currentTime - tStart;

	// Print the output
	if (shouldPrint()) {
		timeOfLastPrint = currentTime;
		print();
	}

	// Check if finished
	if (nItCurrent == nItTot) {
		finalise();
	}
}

bool ProgressBar::shouldPrint(){
	std::chrono::duration<double> timeSinceLastPrint = currentTime - timeOfLastPrint;

    bool isLastFrame = nItCurrent >= nItTot;
    bool isTimeToPrint = timeSinceLastPrint >= std::chrono::duration<double>(1.0 / updateFrequency);
	bool shouldPrint = isLastFrame || isLastFrame || isTimeToPrint;
    return shouldPrint;
}

// TODO: Define the string literals somewhere else
// TODO: Find a way to clear the whole line
// BUG: If the progress string gets shorter, characters at the end are not cleared.
void ProgressBar::print() {
	// Depending on the flag either write new lines each time or refresh current line
	if (deleteCurrentLine) {
		std::cout << "\r";
	} else {
		std::cout << std::endl;
	}

	// Print the progress bar
	int nFullSquares = getCompletionFraction() * progressPrintSquares;
	int nEmptySquares = progressPrintSquares - nFullSquares;

	std::cout << "Progress: |";
	for (int i = 0; i < nFullSquares; i++) {
		std::cout << "#";
	}
	for (int i = 0; i < nEmptySquares; i++) {
		std::cout << " ";
	}
	std::cout << "|";

	// Write it in percentages
	std::cout << " " << (int)(getCompletionFraction() * 100) << "%";

	// Calculate ETA in seconds hours days months and years
	float eta = predictRemainingRunTime().count();

	// Write the ETA
	std::cout << ", ETA: " << createYmwdhmsString(predictRemainingRunTime()) << ".";


	std::cout.flush();
}

void ProgressBar::finalise() {
	// Set the finished flag
	finished = true;

	// Print the final status text
	std::cout << std::endl
			  << "Finished! Total time: " << createYmwdhmsString(getElapsedTime()) << "." << std::endl;
}

std::string ProgressBar::createYmwdhmsString(std::chrono::duration<double> time) {
	// Calculate the time in seconds hours days months and years
	int seconds = time.count();
	int minutes = seconds / 60;
	int hours = minutes / 60;
	int days = hours / 24;
	int months = days / 30;
	int years = months / 12;

    // Make sure the time units wrap back around
    seconds %= 60;
    minutes %= 60;
    hours %= 24;
    days %= 30;
    months %= 12;

	// Write the ETA
	std::stringstream out;
	out << years << "y, " << months << "m, "
		<< days << "d, " << hours << "h, "
		<< minutes << "m, " << seconds << "s";
	return out.str();
}

std::chrono::duration<double> ProgressBar::getAverageTimePerIteration() {
	// Time per iterations so far
	if (getNItCurrent() == 0)
		return std::chrono::duration<double>(0.0);
	else
		return getElapsedTime() / (double)getNItCurrent();
}

std::chrono::duration<double> ProgressBar::nItToPredictedTime(size_t nIt) {
	return nIt * getAverageTimePerIteration();
}

size_t ProgressBar::getNItTot() {
	return nItTot;
}

size_t ProgressBar::getNItCurrent() {
	return nItCurrent;
}

size_t ProgressBar::getNItRemaining() {
	if (nItCurrent > nItTot) {
		return 0;
	} else {
		return nItTot - nItCurrent;
	}
}

std::chrono::duration<double> ProgressBar::getElapsedTime() {
	return elapsedTime;
}

std::chrono::duration<double> ProgressBar::predictTotalRunTime() {
	return nItToPredictedTime(getNItTot());
}

std::chrono::duration<double> ProgressBar::predictRemainingRunTime() {
	return nItToPredictedTime(getNItRemaining());
}

float ProgressBar::getCompletionFraction() {
	return (float)getNItCurrent() / (float)getNItTot();
}

#endif // PROGRESSBAR_H_
