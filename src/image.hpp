#ifndef IMAGE_H_
#define IMAGE_H_

#include "CImg/CImg.h"
#include <string>
#include <vector>

/**
 * An image class to represent images that will be denoised.
 * Essentially a wrapper around the CImg Library.
 *
 * Images are stored in row major format.
 * Coordinate X corresponds to horizontal axis and Y to vertical.
 */
class Image {
private:
	static constexpr int maxColorVal = 255; /** Defines the range of pixel color values. 255 means pixels go from 0 to 255 */
	cimg_library::CImg<float> im; /** CImg object representing the image from CImg library */

public:
	size_t getSizeX(); /** Fetches the horizontal size of the Image */
	size_t getSizeY(); /** Fetches the vertical size of the Image */
	size_t getSizeZ(); /** Fetches the depth (3D or multiframe) of the Image */
	size_t getSizeT(); /** Fetches the spectrum (or RGB channel) size of the Image */

	/**
	 * Fetches the pixel values of the image as a float vector
	 * The ordering of the data is row, column, color.
	 * \return The pixel data as a flost vector
	 */
	std::vector<float> getData();

	/**
	 * Writes the image represneted by the class to disk
	 * \param path the path to which the image is written
	 */
	void writeImage(std::string path);

	/** Default constructor.
	 * Initialises an empty image
	 */
	Image() = default;

	/**
	 * Constructs an image object by reading the image file from a given path
	 * \param path The path from which to read the image data
	 */
	Image(std::string path);

	/**
	 * Construct an image object from a float vector representing the pixel values
	 * BMP is supported natively by CImg. Other formats may have dependencies like imagemagick
	 * The pixel values in the vector should be ordered as row, column and color
	 * \param dataIn the vector containing the pixel values
	 * \param sizeX the horizontal size of the input image
	 * \param sizeY the vertical size of the input image
	 * \param sizeZ the depth size of the input image
	 * \param sizeT the specrtrum size (or RGB Channel count) of the input image
	 */
	Image(const std::vector<float>& dataIn, size_t sizeX, size_t sizeY, size_t sizeZ = 1, size_t sizeT = 3);

	// Defaulted the rest of the constructors and destructors since all member classes have vell defined constructors and desctructors
	~Image() = default;
	Image(const Image&) = default;
	Image(Image&&) = default;
	Image& operator=(const Image&) = default;
	Image& operator=(Image&&) = default;
};

#include <iostream>
#include <vector>

Image::Image(std::string path) {
	// Load the image. It is stored in column, row and rgb order
	im.load(path.c_str());

	// Set the value range to be between 0 and 1
	im /= maxColorVal;

}

Image::Image(const std::vector<float>& dataIn, size_t sizeX, size_t sizeY, size_t sizeZ, size_t sizeT)
	: im(dataIn.data(), sizeX, sizeY, sizeZ, sizeT) {
}

size_t  Image::getSizeX() {
	return im.width();
}

size_t  Image::getSizeY() {
	return im.height();
}

size_t  Image::getSizeZ() {
	return im.depth();
}

size_t  Image::getSizeT() {
	return im.spectrum();
}

std::vector<float> Image::getData() {
	// Copy the raw data over to a vector
	return std::vector<float>(im.begin(), im.end());
}

void Image::writeImage(std::string path) {
	im *= maxColorVal;
    im.save(path.c_str());
	im /= maxColorVal;
}


#endif // IMAGE_H_
