COMP = nvcc
FLAG = 

all: main.o
	$(COMP) $(FLAGS) main.o -o denoise-tv

main.o: src/main.cu
	$(COMP) $(FLAGS) -c src/main.cu --include-path ext/ --define-macro cimg_display=0
